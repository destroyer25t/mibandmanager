﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiBandManager.Helpers
{
    public static class Constants
    {
        public static string XiaomiClientId = "2882303761517154077";
    }

    public static class XiaomiUrlsConstants
    {
        public static string XiaomiAccountUrl = "https://account.xiaomi.com";
        public static string XiaomiSearchUrl = "https://account.huami.com";
        public static string XiaomiLoginUrl = "https://account-us.huami.com";
    }
}
