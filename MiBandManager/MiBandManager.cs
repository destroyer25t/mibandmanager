﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MiBandManager.Helpers;
using RestSharp;

namespace MiBandManager
{
    public class MiBandManager
    {
        private string LoginToken { get; set; }

        /// <summary>
        /// Все операции (отлучение браслета (от церкви) от аккаунта, получение статистики и всего прочего происходит с помощью login_token
        /// Для получения login_token при известных device_id и code нужно выполнить две операции:
        /// Первая: на https://account.huami.com/v1/client/search сделать POST-запрос передав device_id, code и еще некоторые параметры. Вернется accesstoken.
        /// Вторая: на https://account-us.huami.com/v1/client/login сделать POST-запрос передав accesstoken. Вернется login_token.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="thirdTokenId"></param>
        /// <param name="imei"></param>
        public void Authorizate(string userId, string thirdToken, string imei)
        {
            //шаг первый: получаем accesstoken
            string url = $"{XiaomiUrlsConstants.XiaomiSearchUrl}/v1/client/search";

            var client = new RestClient(XiaomiUrlsConstants.XiaomiSearchUrl);
            client.UserAgent = "Dalvik/2.1.0 (Linux; U; Android 6.0.1; D6603 Build/23.5.A.1.291)";

            var request = new RestRequest("v1/client/search", Method.POST);
            request.AddHeader("Accept", "");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            request.AddHeader("Accept-Encoding", "gzip");
            //request.AddHeader("Content-Length", "254");

            request.AddParameter("source", "com.xiaomi.hm.health:3.0.4:3175");
            request.AddParameter("device_id", imei);
            request.AddParameter("third_name", "mi-mifit");
            request.AddParameter("region", "RU");
            request.AddParameter("third_token", thirdToken);
            request.AddParameter("lang", "ru");
            request.AddParameter("device_model", "android_phone");
            request.AddParameter("os_version", "v0.4.2");
            request.AddParameter("app_name", "com.xiaomi.hm.health");
            request.AddParameter("device_id_type", "imei");
            request.AddParameter("", "");
            client.Proxy = new WebProxy("127.0.0.1:8888");

            var resp = client.Execute(request);
        }
    }


}
