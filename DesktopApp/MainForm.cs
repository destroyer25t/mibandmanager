﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiBandManager = MiBandManager.MiBandManager;

namespace DesktopApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void bXiaomiId_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbXiaomiId.Text) || String.IsNullOrWhiteSpace(tbXiaomiId.Text))
            {
                lError.Text = "Id может состоять только из цифр!";
                lError.Visible = true;
            }
            else
            {
                using (var b = new BrowserForm(tbXiaomiId.Text))
                {
                    var result = b.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        pnlLogin.SendToBack();
                        pnlMain.BringToFront();

                        global::MiBandManager.MiBandManager mnBandManager = new global::MiBandManager.MiBandManager();
                        mnBandManager.Authorizate(tbXiaomiId.Text, b.ThirdPartyId, tbIMEI.Text);
                    }
                    else
                    {
                        MessageBox.Show("Не удалось получить данные для дальнейших операций.");
                    }
                }
            }
        }

        private void tbXiaomiId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbXiaomiId_Click(object sender, EventArgs e)
        {
            lError.Text = "";
            lError.Visible = false;
        }
    }
}
