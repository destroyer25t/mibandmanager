﻿using System;
using System.IO;
using System.Windows.Forms;
using Gecko;

namespace DesktopApp
{
    public partial class BrowserForm : Form
    {
        private readonly GeckoWebBrowser _webBrowser;
        private string userId = "";

        public string ThirdPartyId;

        public BrowserForm(string xiaomiId)
        {
            InitializeComponent();
            var appDir = Path.GetDirectoryName(Application.ExecutablePath);
            Xpcom.Initialize(appDir);
            _webBrowser = new GeckoWebBrowser
            {
                Dock = DockStyle.Fill,
                Name = "browser"
            };
            this._webBrowser.DocumentCompleted += this._webBrowser_DocumentCompleted;
            userId = xiaomiId;
            Controls.Add(_webBrowser);
        }

        private void BrowserForm_Load(object sender, EventArgs e)
        {
            string url = $"https://account.xiaomi.com/oauth2/authorize?skip_confirm=false&client_id={MiBandManager.Helpers.Constants.XiaomiClientId}&scope=1+2&redirect_uri=https%3A%2F%2Fhm.xiaomi.com%2Fhuami.health.loginview.do&_locale=ru_RU&response_type=code&userId={userId}&confirmed=true\r\n";
            _webBrowser.Navigate(url);
        }

        private void _webBrowser_DocumentCompleted(object sender, EventArgs e)
        {
            string webBrowserUrl = _webBrowser.Url.ToString();
            
            //Получаем AWS из url
            if (webBrowserUrl.Contains("AWS")&& string.IsNullOrEmpty(ThirdPartyId))
            {
                try
                {
                    ThirdPartyId = webBrowserUrl.Split(new string[] { "code=" }, StringSplitOptions.None)[1];
                    this.DialogResult = DialogResult.OK;
                }
                catch (IndexOutOfRangeException exception)
                {
                    MessageBox.Show("Возникло исключение при попытке извлечь параметр из URL. Изменился формат возвращаемого ответа. " + exception.Message);
                    this.DialogResult = DialogResult.Abort;
                    this.Close();
                }
            }

            if (!String.IsNullOrEmpty(ThirdPartyId)) // && !String.IsNullOrEmpty(DeviceId))
            {
                this.Close();
            }
        }
    }
}
