﻿namespace DesktopApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlLogin = new System.Windows.Forms.Panel();
            this.lError = new System.Windows.Forms.Label();
            this.bXiaomiId = new System.Windows.Forms.Button();
            this.tbXiaomiId = new System.Windows.Forms.TextBox();
            this.lUserId = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.lImei = new System.Windows.Forms.Label();
            this.tbIMEI = new System.Windows.Forms.TextBox();
            this.pnlLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLogin
            // 
            this.pnlLogin.Controls.Add(this.tbIMEI);
            this.pnlLogin.Controls.Add(this.lImei);
            this.pnlLogin.Controls.Add(this.lError);
            this.pnlLogin.Controls.Add(this.bXiaomiId);
            this.pnlLogin.Controls.Add(this.tbXiaomiId);
            this.pnlLogin.Controls.Add(this.lUserId);
            this.pnlLogin.Location = new System.Drawing.Point(13, 13);
            this.pnlLogin.Name = "pnlLogin";
            this.pnlLogin.Size = new System.Drawing.Size(826, 478);
            this.pnlLogin.TabIndex = 0;
            // 
            // lError
            // 
            this.lError.AutoSize = true;
            this.lError.ForeColor = System.Drawing.Color.OrangeRed;
            this.lError.Location = new System.Drawing.Point(318, 269);
            this.lError.Name = "lError";
            this.lError.Size = new System.Drawing.Size(35, 13);
            this.lError.TabIndex = 3;
            this.lError.Text = "афыа";
            this.lError.Visible = false;
            // 
            // bXiaomiId
            // 
            this.bXiaomiId.Location = new System.Drawing.Point(381, 245);
            this.bXiaomiId.Name = "bXiaomiId";
            this.bXiaomiId.Size = new System.Drawing.Size(75, 23);
            this.bXiaomiId.TabIndex = 2;
            this.bXiaomiId.Text = "Войти";
            this.bXiaomiId.UseVisualStyleBackColor = true;
            this.bXiaomiId.Click += new System.EventHandler(this.bXiaomiId_Click);
            // 
            // tbXiaomiId
            // 
            this.tbXiaomiId.Location = new System.Drawing.Point(304, 184);
            this.tbXiaomiId.Name = "tbXiaomiId";
            this.tbXiaomiId.Size = new System.Drawing.Size(229, 20);
            this.tbXiaomiId.TabIndex = 1;
            this.tbXiaomiId.Click += new System.EventHandler(this.tbXiaomiId_Click);
            this.tbXiaomiId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbXiaomiId_KeyPress);
            // 
            // lUserId
            // 
            this.lUserId.AutoSize = true;
            this.lUserId.Location = new System.Drawing.Point(248, 187);
            this.lUserId.Name = "lUserId";
            this.lUserId.Size = new System.Drawing.Size(53, 13);
            this.lUserId.TabIndex = 0;
            this.lUserId.Text = "Xiaomi Id:";
            // 
            // pnlMain
            // 
            this.pnlMain.Location = new System.Drawing.Point(12, 13);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(827, 478);
            this.pnlMain.TabIndex = 1;
            // 
            // lImei
            // 
            this.lImei.AutoSize = true;
            this.lImei.Location = new System.Drawing.Point(269, 213);
            this.lImei.Name = "lImei";
            this.lImei.Size = new System.Drawing.Size(32, 13);
            this.lImei.TabIndex = 4;
            this.lImei.Text = "IMEI:";
            // 
            // tbIMEI
            // 
            this.tbIMEI.Location = new System.Drawing.Point(304, 210);
            this.tbIMEI.Name = "tbIMEI";
            this.tbIMEI.Size = new System.Drawing.Size(229, 20);
            this.tbIMEI.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 503);
            this.Controls.Add(this.pnlLogin);
            this.Controls.Add(this.pnlMain);
            this.Name = "MainForm";
            this.Text = "MiBand Manager";
            this.pnlLogin.ResumeLayout(false);
            this.pnlLogin.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLogin;
        private System.Windows.Forms.Button bXiaomiId;
        private System.Windows.Forms.TextBox tbXiaomiId;
        private System.Windows.Forms.Label lUserId;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Label lError;
        private System.Windows.Forms.TextBox tbIMEI;
        private System.Windows.Forms.Label lImei;
    }
}